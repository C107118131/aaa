from flask import Flask, render_template, request, session, flash, redirect, url_for, request
from database import db_session, init_db
from models.user import User
app =  Flask(__name__)

@app.before_first_request
def init():
    init_db()#初始化資料庫

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()#每次request後面就會移除資料庫的session


@app.route('/')#呼叫路徑若為跟目錄(/)
def start():
    return 'welcome!'

@app.route('/base.html/')
def asd():
    return render_template("base.html")


@app.route('/create-user.html/',methods=['GET','POST'])#可以同時接受兩種方法
def register():
    #如果用post方法 傳表單給後臺處裡 # GET 跟後端伺服器取得網頁頁面
    if request.method is 'POST':
        name = request.form.get("name")
        identify = request.form.get("identify")
        password= request.form.get("password")
        email = request.form.get("email")
        phone = request.form.get("phone")

        user = User(
            name = name,
            identify = identify,
            password = password,
            email = email,
            phone = phone
        )#接收到表單物件就可以新增一個物件
        db_session.add(user)#將user加入表單
        db_session.commit()#存入

        return "{}, {}, {}, {}, {}".format(name, identify, password, email, phone)

    return render_template("/create-user.html/")#回傳template下的create-user.html


@app.route('/user.html/')
def user_list():
    users = User.query.all()#抓出所有使用者，存入users變數中

    return render_template('user.html',users=users)#呼叫user.html並帶入所有users

@app.route('/signin.html/',methods=['GET','POST'])
def signin():
    
    user = User.query.filter(User.identify == request.form.get("identify")).first()#  當使用者按下signin之後，先檢核帳號是否存在系統內

    if user:
        if User.passward is request.form.get("password"):#  當使用者存在資料庫內再核對密碼是否正確
            return redirect("signin.html")
    else: 
        return 'Error 404'

#是否是直接被呼叫的(main)) 若為True則請SERVER找出錯誤
if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.run(
        host='127.0.0.1',
        port=5555,
        debug=True
    )



