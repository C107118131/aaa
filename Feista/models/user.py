from sqlalchemy import Column, Integer, String
from database import Base
import uuid

class User(Base):
    __tablename__ = 'user'#會存在資料庫裡面的table的名稱
    id = Column(String(50), primary_key=True, nullable=True)#primary_key=True 排序
    name = Column(String(50), nullable=True)
    identify = Column(String(120), unique=True, nullable=True)#unique=True 不能重複
    password = Column(String(120), nullable=True)
    email = Column(String(120), unique=True, nullable=True)#nullable=True 不得為空值
    phone = Column(String(50), unique=True, nullable=True)


    def __init__(self, name, identify, password, email, phone):#建立物件 初始化
        self.id = str(uuid.uuid4())#創造id(uuid:通過隨機數來生成UUID. 使用的是偽隨機數有一定的重複概率. )
        self.name = name
        self.identify = identify
        self.password = password
        self.email = email
        self.phone = phone

    def __repr__(self):#傳遞物件
        return '<User %r>' % (self.name)