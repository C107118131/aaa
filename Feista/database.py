from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import os


current_dir = os.path.dirname(__file__)#取得目前路徑
engine = create_engine('sqlite:///{}/fiesta.db'.format(current_dir), convert_unicode=True)#建立資料庫檔案sqlite

db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))#設定session

Base = declarative_base()#創建資料表
Base.query = db_session.query_property()#詢問屬性

#初始化
def init_db():
    Base.metadata.create_all(bind=engine)#新增表單
    print("successfully")#確認連線